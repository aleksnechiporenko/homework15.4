﻿
#include <iostream>

//Функция принимает 2 параметра - a и b 
//Если a == true (1) перечисляет все нечетные числа от 0 до b
//Если a == false (0) перечисляет четные числа от 0 до b
void func(bool a, int b)
{
    const int n = b;

    for (int i = a; i <= n; i += 2)
        {
            std::cout << i << "\n";
        }
  


}

int main()
{
    const int n = 28;
   
    //Выводит все четные числа от 0 до n
    for (int i = 0; i <= n; ++i)
    {
        if (i % 2 == 0) 
        {
            std::cout << i << "\n";
        }
        
    }

    std::cout << "\n";

    func(true, 25);
}

